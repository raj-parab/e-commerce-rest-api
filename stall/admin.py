from django.contrib import admin
from .models import Client, Customer, Category, Product, Coupon, Cart, Payment

admin.site.register([Client, Customer, Category, Product, Coupon, Cart, Payment])
