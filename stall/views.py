from django.shortcuts import render
from rest_framework import viewsets

from django.http import JsonResponse
from .serializer import (ClientSerializer,
						 CustomerSerializer,
						 CartSerializer,
						 CategorySerializer,
						 CouponSerializer,
						 ProductSerializer,
						 PaymentSerializer)


from .models import (Client, Customer, Category, Product, Coupon, Cart, Payment)

def checkLogin(request, contact_no):
	try:
		client=Client.objects.get(contact_no=contact_no)
	except BaseException as e:
		return JsonResponse({'status':'failure','details':f"client with{contact_no} doesn't exist"})

	else:
		return JsonResponse({'status':'success', 'details':{'client_id':client.client_id}})


class ClientViewSet(viewsets.ModelViewSet):
	serializer_class = ClientSerializer
	queryset = Client.objects.all()


class CustomerViewSet(viewsets.ModelViewSet):
	serializer_class = CustomerSerializer
	queryset = Customer.objects.all()


class CartViewSet(viewsets.ModelViewSet):
	serializer_class = CartSerializer
	queryset = Cart.objects.all()


class CouponViewSet(viewsets.ModelViewSet):
	serializer_class = CouponSerializer
	queryset = Coupon.objects.all()


class CategoryViewSet(viewsets.ModelViewSet):
	serializer_class = CategorySerializer
	queryset = Category.objects.all()


class ProductViewSet(viewsets.ModelViewSet):
	serializer_class = PaymentSerializer
	queryset = Product.objects.all()


class PaymentViewSet(viewsets.ModelViewSet):
	serializer_class = PaymentSerializer
	queryset = Payment.objects.all()


