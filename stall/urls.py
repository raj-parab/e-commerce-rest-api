
from django.contrib import admin
from django.urls import path

from rest_framework.routers import DefaultRouter
from .views import (ClientViewSet,
					CustomerViewSet,
					CartViewSet,
					CouponViewSet,
					CategoryViewSet,
					ProductViewSet,
					PaymentViewSet,
					checkLogin)
urlpatterns = [path("checkLogin/<str:contact_no>/", checkLogin, name='check-login')]


router = DefaultRouter()
router.register('client', ClientViewSet, basename='client')
router.register('customer', CustomerViewSet, basename='customer')
router.register('cart', CartViewSet, basename='cart')
router.register('coupon', CouponViewSet, basename='coupon')
router.register('category', CategoryViewSet, basename='category')
router.register('product', ProductViewSet, basename='product')
router.register('payment', PaymentViewSet, basename='payment')

urlpatterns+=router.urls



   